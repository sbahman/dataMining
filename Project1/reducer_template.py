#!/local/anaconda/bin/python
# IMPORTANT: leave the above line as is.

import numpy as np
import sys


def print_duplicates(videos):
    unique = np.unique(videos)
    for i in xrange(len(unique)):
        for j in xrange(i + 1, len(unique)):
            print "%d\t%d" % (min(unique[i], unique[j]),
                              max(unique[i], unique[j]))

last_key = None
last_hash = None
key_count = 0
duplicates = []

for line in sys.stdin:
    line = line.strip()
    key, hash_b, video_id = line.split("\t")

    if last_key is None:
        last_key = key
        last_hash = hash_b

    if (key == last_key) and (hash_b == last_hash):
            duplicates.append(int(video_id))
    else:
        # Key changed or hash-value changed (previous line was k=x, this line is k=y)
        print_duplicates(duplicates)
        duplicates = [int(video_id)]
        last_key = key
        last_hash = hash_b

if len(duplicates) > 0:
    print_duplicates(duplicates)
