#!/local/anaconda/bin/python
# IMPORTANT: leave the above line as is.

import numpy as np
np.set_printoptions(threshold='nan')
import sys

def minHash(sh_matrix, permute_coeff):
    signature = np.zeros([c, 1])
    signature[:] = np.inf
    signature = np.amin(np.matrix(permute_coeff) * np.matrix(sh_matrix) % nprime, axis = 1)
    return signature

def bandHash(b, signature, a_v):
    nprime1 = 1002583
    c = signature.shape[0]
    if (c % b != 0):
        print ('Error! c % b != 0')
    else:
        hashes = np.zeros(b)
        for i in range(0, b):
            sign_slice = signature[i*r : (i+1)*r , :]
            x = np.append(np.array(sign_slice), [1])
            hashes[i] = np.dot(x, a_v[i, :]) % nprime1
    return hashes

if __name__ == "__main__":
    # VERY IMPORTANT:
    # Make sure that each machine is using the
    # same seed when generating random numbers for the hash functions.
    N = 20000       # number of shingles
    nprime = 20011  # first prime next to N
    c = 950
    b = 50         # number of bands
    r = c / b

    np.random.seed(seed=42)
    permute_coeff = np.random.random_integers(2**10, 2**30, [c,2])
    a_v = np.random.random_integers(2**10, 2**30, [b, r+1])

    for line in sys.stdin:
        line = line.strip()
        video_id = int(line[6:15])
        shingles = np.fromstring(line[16:], sep=" ", dtype = int)

        sh_matrix = np.vstack([shingles, np.ones([1, len(shingles)])])

        signature = minHash(sh_matrix, permute_coeff)
        hashes = bandHash(b, signature, a_v)

        for i in range(0, b):
            print '%02d %05d\t%03d %s' % (int(i), int(hashes[i]), video_id, ' '.join([str(a) for a in shingles]))