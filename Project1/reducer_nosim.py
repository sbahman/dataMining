#!/local/anaconda/bin/python
# IMPORTANT: leave the above line as is.

import numpy as np
import sys


def similarity(x, y):
    return float(sum([a & b for (a, b) in zip(x, y)]))/float(sum([a | b for (a, b) in zip(x, y)]))


def print_duplicates(videos_vector):
    videos = np.unique(videos_vector)
    #videos = videos_vector.keys()
    #print videos_vector
    for i in xrange(len(videos)):
        for j in xrange(i + 1, len(videos)):
            #sim = similarity(videos_vector[videos[i]], videos_vector[videos[j]])
            #print videos_vector[videos[i]], videos_vector[videos[j]]
            #print sim
            #if sim>=0.9:
                #print(videos_vector[videos[i]], videos_vector[videos[j]])
                print "%d\t%d" % (min(videos[i], videos[j]),
                                  max(videos[i], videos[j]))

last_key = None
last_hash = None
key_count = 0
duplicates = []

for line in sys.stdin:
    line = line.strip()
    key, hash_b, video_id = line.split("\t")
    #video_vector = [bool(int(x)) for x in video_shingles.split(' ')]
    # print video_shingles
    if last_key is None:
        last_key = key
        last_hash = hash_b

    if (key == last_key) and (hash_b == last_hash):
            duplicates.append(int(video_id))
    else:
        # Key changed or hash-value changed (previous line was k=x, this line is k=y)
        print_duplicates(duplicates)
        duplicates = [int(video_id)]
        last_key = key
        last_hash = hash_b

if len(duplicates) > 0:
    print_duplicates(duplicates)
