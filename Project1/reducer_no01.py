#!/local/anaconda/bin/python
# IMPORTANT: leave the above line as is.

import numpy as np
import sys


def similarity(x, y):
    return float(len(x.intersection(y)))/float(len(x.union(y)))


def print_duplicates(videos_vector):
    videos = videos_vector.keys()
    for i in xrange(len(videos)):
        for j in xrange(i + 1, len(videos)):
            sim = similarity(videos_vector[videos[i]], videos_vector[videos[j]])
            if sim>=0.9:
                print "%d\t%d" % (min(videos[i], videos[j]),
                                  max(videos[i], videos[j]))

last_band = None
last_hash = None
key_count = 0
duplicates = {}

for line in sys.stdin:
    line = line.strip()
    (key, value) = line.split("\t")
    band_num = int(key.split(' ')[0])
    hash_b = int(key.split(' ')[1])
    video_id = int(value.split(' ')[0])
    video_vector = [int(x) for x in value.split(' ')[1:]]
    # print video_shingles
    if last_band is None:
        last_band = band_num
        last_hash = hash_b

    if (band_num == last_band) and (hash_b == last_hash):
            duplicates[(int(video_id))] = set(video_vector)
    else:
        # Key changed or hash-value changed (previous line was k=x, this line is k=y)
        print_duplicates(duplicates)
        duplicates = {}
        duplicates[int(video_id)] = set(video_vector)
        last_band = band_num
        last_hash = hash_b

if len(duplicates) > 0:
    print_duplicates(duplicates)
