#!/local/anaconda/bin/python
# IMPORTANT: leave the above line as is.

import numpy as np
np.set_printoptions(threshold='nan')
import sys

def hashVector(c):
    # nprime = N+1
    x = range(0, nprime)
    x_ones = np.ones([1, nprime])
    x_matrix = np.matrix([x, x_ones[0,:]], dtype = int)
    ab = np.matrix(np.random.random_integers(2**10, 2**30, [c,2]))
    # print ab
    m1 = ((ab* x_matrix) % nprime)
    # m2 = np.zeros([c,N], dtype = int)
    # for i in range(0, #m1.shape[0]):
    #     x = m1[i,:]
    #     y = [a for a in x.tolist()[0] if (a < N)]
    #     m2[i,:] = y
    return m1

def minHash(shingles_index, permute):
    # permute = hashvector(c)
    #c = permute.shape[0]
    # print permute
    signature = np.zeros([c, 1])
    signature[:] = np.inf
    idx = shingles_index.nonzero()
    for i in idx[0]:
        hashes = permute[:, i]
        for k in range(0, c):
            signature[k, 0] = min(hashes[k], signature[k,0])
    return signature

def bandHash(b, signature, a_v):
    nprime1 = 1002583
    c = signature.shape[0]
    if (c % b != 0):
        print ('Error! c % b != 0')
    else:
        hashes = np.zeros(b)
        for i in range(0, b):
            sign_slice = signature[i*r : (i+1)*r , :]
            #a_v = np.random.random_integers(2**10, 2**30, [1, r+1])
            x = np.append(sign_slice, [1])
            hashes[i] = np.dot(x, a_v[i, :]) % nprime1
            #hashes[i] = sum(x) % nprime
    return hashes





if __name__ == "__main__":
    # VERY IMPORTANT:
    # Make sure that each machine is using the
    # same seed when generating random numbers for the hash functions.
    N = 20000       # number of shingles
    #N = 23
    nprime = 20011  # first prime next to N
    #nprime = 23
    #c = 400       # number of hash-functions
    c = 500
    b = 20         # number of bands
    r = c / b

    np.random.seed(seed=42)
    permute = hashVector(c)
    a_v = np.random.random_integers(2**10, 2**30, [b, r+1])

    for line in sys.stdin:
        #np.random.seed(seed=36)
        # print line
        line = line.strip()
        video_id = int(line[6:15])
        shingles = np.fromstring(line[16:], sep=" ", dtype = int)
        #print shingles
        # key = video_id
        shingles_index = np.zeros(N, dtype = int)
        shingles_index[shingles] = 1
        signature = minHash(shingles_index, permute)
        hashes = bandHash(b, signature, a_v)

        for i in range(0, b):
            #print '%02d\t%05d\t%03d\t%s' % (int(i), int(hashes[i]), video_id, ' '.join([str(a) for a in shingles_index]))
            print '%02d\t%05d\t%03d' % (int(i), int(hashes[i]), video_id)

       # print '%d\t%s' % (key, np.array_str(shingles_index))
        # print (key, shingles_index)

