How to execute the whole program:

$ cat data/training.txt | python mapper.py | sort |python reducer.py >> output_c_b.txt
where	c - number of hash-functions
	b - number of bands (both hardcoded in the mapper.py, so change it there)

Now - 2 versions of mapper and reducer: with direct similarity (mapper.py and reducer.py) and without (mapper_nosim.py, reducer_nosim.py).
Use them only together, and don't mix!

Check reported values:

$ python check.py output_c_b.txt duplicates.txt

