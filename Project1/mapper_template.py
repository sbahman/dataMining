#!/local/anaconda/bin/python
# IMPORTANT: leave the above line as is.

import numpy as np
np.set_printoptions(threshold='nan')
import sys

if __name__ == "__main__":
    # VERY IMPORTANT:
    # Make sure that each machine is using the
    # same seed when generating random numbers for the hash functions.
    np.random.seed(seed=42)

    for line in sys.stdin:
        # print line
        line = line.strip()
        video_id = int(line[6:15])
        shingles = np.fromstring(line[16:], sep=" ", dtype = int)
        #print shingles
        key = video_id
        shingles_index = np.zeros(20000, dtype = int)
        shingles_index[shingles] = 1
        print '%d\t%s' % (key, np.array_str(shingles_index))
        # print (key, shingles_index)

