#!/local/anaconda/bin/python
# IMPORTANT: leave the above line as is.

import sys
import numpy as np
from sklearn import linear_model


DIMENSION = 400  # Dimension of the original data.
CLASSES = (-1, +1)   # The classes that we are trying to predict.


np.random.seed(seed=42)
m = 8000  # length of new vector
omega = np.random.randn(DIMENSION, m)
c = np.random.rand(1, m)*(2*np.pi)


def transform(x_original):
    # np.random.seed(seed=420)
    # m = 400  # length of new vector
    # b = np.random.rand(1, m)*(2*np.pi)
    # print x_original.shape
    x = np.sqrt(x_original)
    #x = np.array(x_original, ndmin=2)
    # print x.shape, omega.shape
    DIM = x.shape[0]
    cosarg = np.add(np.dot(x, omega), c)
    f = np.sqrt(2)*np.cos(cosarg) / np.sqrt(m)
    # print f[0].shape
    return f[0]

if __name__ == "__main__":
    sample = None
    labels = []

    n = 5       # size of a subsapmle
    C = 1e-5     # lambda
    i = 0       # to keep track of the current batch line
    b = 0       # to keep track of the current batch number
    w = None
    count = 0

    for line in sys.stdin:
        count += 1
        # print count
        line = line.strip()
        (label, x_string) = line.split(" ", 1)
        label = int(label)

        # x = np.fromstring(x_string, sep = ' ')
        x_original = np.fromstring(x_string, sep=' ')
        x = transform(x_original)  # Use our features.
        # print x.shape
        # print x

        # when data is arriving, we just collect it
        if sample is None:
            sample = x
            labels = [label]
            clf = linear_model.SGDClassifier(alpha=C, fit_intercept=False)
            i += 1
        elif i < n:
            sample = np.vstack([sample, x])
            labels.append(label)
            i += 1
        if i == int(n):
            b += 1
            labels = np.array(labels, ndmin=1)
            # sample0 = transform(sample)
            clf.partial_fit(sample, labels, classes=CLASSES)
            # w = compute_w(w, sample, labels, b)
            sample = x
            labels = [label]
            i = 0

    w = clf.coef_[0]
    # print w (that is only w_T+1 obtained from the last observation of the batch)
    print '%d\t%s' % (1, ' '.join(str(float(x)) for x in w))
