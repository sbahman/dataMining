
import logging
import sys
import numpy as np

data_file = "visual_test_set.csv"
data = np.loadtxt(open(data_file, "rb"))
n=np.shape(data)[0]

predict=np.zeros((n,1))

if __name__ == "__main__":
    for line in sys.stdin:
        line = line.strip()
        w = np.fromstring(line, sep=' ')
    
    for i in range(0,n):
        predict[i,]=cmp(np.dot(data[i,],w),0)
        

    np.savetxt('prediction.csv',predict,delimiter=',')