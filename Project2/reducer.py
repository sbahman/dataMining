
import logging
import sys
import numpy as np

if __name__ == "__main__":
    s = None
    n = 0.0
    for line in sys.stdin:
        line = line.strip()
        (key, value) = line.split('\t')
        w = np.fromstring(value, sep=' ')
        if s is None:
            s = w
            n = 1
        else:
            s = np.add(s, w)
            n += 1

    s_out = s / n
    print(' '.join(str(float(x)) for x in s_out))