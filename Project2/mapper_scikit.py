#!/local/anaconda/bin/python
# IMPORTANT: leave the above line as is.

import sys
import numpy as np
from sklearn import linear_model


DIMENSION = 400  # Dimension of the original data.
CLASSES = (-1, +1)   # The classes that we are trying to predict.

def transform(x_original):
	#xsq = geQuadFeat(x_original)
	x = np.hstack([np.sqrt(x_original), np.array([1])])
	return x

if __name__ == "__main__":
    sample = None
    labels = []

    n = 200       # size of a subsapmle - 1
    C = 1e-4     # lambda
    i = 0       # to keep track of the current batch line
    b = 0       # to keep track of the current batch number
    w = None
    count = 0

    for line in sys.stdin:
        count+= 1
        line = line.strip()
        (label, x_string) = line.split(" ", 1)
        label = int(label)
        x_original = np.fromstring(x_string, sep=' ')
        x = transform(x_original)  # Use our features.

        # when data is arriving, we just collect it
        if sample is None:
            sample = x
            labels = [label]
            clf = linear_model.SGDClassifier(alpha=C, fit_intercept=False)
            i += 1
        elif i < n:
            sample = np.vstack([sample, x])
            labels.append(label)
            i += 1
        if i == int(n):
            b += 1
            labels = np.array(labels,ndmin=1)
            clf.partial_fit(sample, labels, classes=CLASSES)
            #w = compute_w(w, sample, labels, b)
            sample = x
            labels = [label]
            i = 0


    w = clf.coef_[0]
    #print w (that is only w_T+1 obtained from the last observation of the batch)
    print '%d\t%s' % (1, ' '.join(str(float(x)) for x in w))
