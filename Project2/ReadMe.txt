mapper.py - stochastic gradient descent
mapper_AdaGrad.py - SGD with AdaGrad algorithm

Feel free to vary n in C in the main script! My findings are: n is around 50-200,
C is around 1e-3 - 1e-4

Make sure that transformation occurs ONLY in function transform(), WITHOUT any additional function calls.

NOTE that transform() function from function called "mapper.py" is imported into evaluation script!! So, either transform function should be the same on both mappers or change the name of mapper function in evaluate.py

Creating of weights vector is as usual:
cat data/training.txt | python mapper.py | sort | python reducer.py >> my_weights.txt

Caluclation of training error is as follows:
python evaluate.py my_weights.txt data/training_data.txt data/training_labels.txt

divider.py is used for creating of labels.txt and data.txt, which are needed for evaluation. 
NO separation between train and test data yet.



