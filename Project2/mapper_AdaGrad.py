#!/local/anaconda/bin/python
# IMPORTANT: leave the above line as is.

import sys
import numpy as np

DIMENSION = 400  # Dimension of the original data.
CLASSES = (-1, +1)   # The classes that we are trying to predict.

def transform(x_original):
	#xsq = getQuadFeat(x_original)
	x = np.hstack([np.sqrt(x_original) + x_original, np.array([1])])
	return x

def compute_w(w_old, sample, labels, t, Gold):
    eta = 1.0/(C*t)

    z = np.dot(sample, w_old)
    indices = [i for i in range(0, int(n)) if z[i]*labels[i] < 1]

    # Convert labels int 2D array
    labels = np.array(labels, ndmin=2).T

    #nabla = C*w_old - 1.0/n * np.dot((sample[indices,:]).T, np.array([labels[i] for i in indices], ndmin=2).T)
    # Define the gradient for batch b (which is actuall t in the slides)
    grad_t = C*w_old - eta/n * np.dot((sample[indices,:]).T, labels[indices])
    gradprod = np.square(grad_t)
    # Achtung Gnew is G^2 in lecture slides
    Gnew = np.add(Gold, gradprod.T)
    Gsqrt = np.sqrt(Gnew)
    Ginv = np.reciprocal(Gsqrt)

    w_old = np.add(w_old, -eta*np.multiply(Ginv, grad_t.T).T)
    # Not sure whether this projection is correct, was just an educated guess
    #(as the Mahalonobis norm is just the dot product between w-w', G*(w-w'), so we do the same thing as before, only with Gw')

    #print Gsqrt.shape, w_old.shape

    x1 = np.multiply(Gsqrt.T, w_old)
    x2 = np.dot(x1.T, w_old)

    w_new = w_old * min(1, float(1)/(np.sqrt(C)*np.sqrt(x2[0,0])))

    #print Gnew
    # for j in range(0, len(labels)):
    #     x_t = sample[j, :]
    #     y_t = labels[j]
    #     if np.dot(x_t, w)[0]*y_t < 1:
    #         w = np.add(w.T, eta*y_t*x_t).T
    #         w = w*min(1, 1/np.sqrt(C)/np.linalg.norm(w))
    return (w_new, Gnew)

if __name__ == "__main__":
    sample = None
    labels = []

    n = 200       # size of a subsapmle
    C = 1e-4     # lambda
    i = 0       # to keep track of the current batch line
    b = 0       # to keep track of the current batch number
    w = None
    count = 0

    for line in sys.stdin:
        count+= 1
        line = line.strip()
        (label, x_string) = line.split(" ", 1)
        label = int(label)
        x_original = np.fromstring(x_string, sep=' ')
        x = transform(x_original)  # Use our features.

        #if count % 1000==0:
         #   print count
        
        # when data is arriving, we just collect it
        if sample is None:
            sample = x
            labels = [label]
            w = np.zeros([x.shape[0], 1])
            G = np.ones([1, x.shape[0]])
            i += 1
        elif i < n:
            sample = np.vstack([sample, x])
            labels.append(label)
            i += 1
        if i == int(n):
            b += 1
            (w, G) = compute_w(w, sample, labels, b, G)
            #print G
            sample = x
            labels = [label]
            i = 0
    #print w (that is only w_T+1 obtained from the last observation of the batch)
    print '%d\t%s' % (1, ' '.join(str(float(x)) for x in w))